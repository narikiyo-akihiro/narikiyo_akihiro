<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
	<h2>新規投稿画面</h2>
	<div class="main-contents">
		<div class="header">
			<div class="form-area">
				<form action="newMessage" method="post">
					件名<br />
					<textarea name="subject" cols="30" class="subject-box"></textarea>
					<br />カテゴリー<br />
					<textarea name="category" cols="10" class="category-box"></textarea>
					<br />本文<br />
					<textarea name="text" cols="50" class="text-box"></textarea>
					<br /> <input type="submit" value="投稿">
				</form>
			</div>
			<br /> <a href="home">戻る</a>
		</div>
		<div class="copyright">Copyright(c)YourName</div>
	</div>
</body>
</html>