<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
	<h2>ユーザー管理画面</h2>

	<div class="header">
		<div class="main-contents">
			<a href="registration">新規登録</a> <a	href="home">戻る</a>
		</div>
	</div>

	<table border="1">
		<thead>
			<tr>
				<th>ログインID</th>
				<th>名称</th>
				<th>支店</th>
				<th>部署・役職</th>
				<th>編集</th>
				<th>停止・再開</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${users}" var="user">
				<tr>
					<td><div class="account-name"><span class="login"><c:out value="${user.login}" /></span></div></td>
					<td><div class="name"><c:out value="${user.name}" /></div></td>
					<td><div class="branch_name"><c:out value="${user.branch_name}" /></div></td>
					<td><div class="position_name"><c:out value="${user.position_name}" /></div></td>
					<td><a href="settings?id=${user.id}">編集</a></td>
					<td>
					<c:if test="${user.status==0}">
					<form action="management" method="post">
					    <button type="submit" name="status" value="1">停止</button>
					    <input name="id" value="${user.id}"  type="hidden"/></form></c:if>

					<c:if test="${user.status==1}">
					<form action="management" method="post">
					　　<button type="submit" name="status" value="0">再開</button>
				    	<input name="id" value="${user.id}"  type="hidden"/></form></c:if></td>
					</tr>
			</c:forEach>
			</table>

			<div class="copyright">Copyright(c)YourName</div>
</body>
</html>
