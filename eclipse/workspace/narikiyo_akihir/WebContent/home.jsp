<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<h2>ホーム画面</h2>
			<div class="profile">
				<div class="name">
					<c:out value="${loginUser.name}" />
					さん 今日も1杯いかがですか？
				</div>
				<a href="post">新規投稿</a> <a href="management">ユーザー管理</a> <a
					href="logout">ログアウト</a>

			</div>
		</div>
		<h3>投稿一覧</h3>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="subject">
						<c:out value="${message.subject}" />
					</div>
					<div class="text">
						<c:out value="${message.text}" />
					</div>
					<div class="category">
						<c:out value="${message.category}" />
					</div>
					<div class="date">
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<br>

					<div class="form-area">
						<form action="newComment" method="post">
							<textarea name="comment" cols="50" class="comment-box"></textarea>
							<br /> <input type="submit" value="コメント">（500文字まで） <input
								name="automessage" value="${message.id}" id="id" type="hidden" />
						</form>
					</div>
					<br>

					<div class="comments">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${comment.messageId==message.id}">
								<div class="comment">
									<div class="account-name">
										<span class="name"><c:out value="${comment.name}" /></span>
									</div>
									<div class="text">
										<c:out value="${comment.text}" />
									</div>
									<div class="date">
										<fmt:formatDate value="${comment.created_date}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
								</div>
					          </c:if>
							 <c:if test="${comment.delete==0}">
					            <form action="home" method="post">
					            <button type="submit" name="delete" value="1">削除</button>
					    　　　　<input name="id" value="${message.id}"  type="hidden"/></form>
					         </c:if>

						</c:forEach>
					</div>
					<br>
				</div>
			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)YourName</div>

	</div>
</body>
</html>
