<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.name}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h2>編集画面</h2>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${editUser.id}" id="id" type="hidden" />
			<label for="login">ログインID</label> <input name="login" value="${editUser.login}" /><br />

		    <label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />

		    <label for="name">名称</label> <input name="name" value="${editUser.name}" /><br />

            支店<select name="branch"> <c:forEach items="${branches}" var="branch">
                <option value="${branch.branch_id}"<c:if test="${ branch.branch_id == editUser.branch }">selected</c:if>>
                ${branch.branch_name} </option> </c:forEach> </select><br />

			部署･役職<select name="position"> <c:forEach items="${positions}" var="position">
			        <option value="${position.position_id}"<c:if test="${ position.position_id == editUser.position }">selected</c:if>>
					${position.position_name} </option> </c:forEach> </select> <br />

				    <input type="submit" value="登録" /> <br /> <a href="management">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>