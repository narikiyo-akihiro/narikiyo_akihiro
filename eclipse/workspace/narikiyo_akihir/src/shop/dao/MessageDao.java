package shop.dao;

import static shop.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import shop.beans.Message;
import shop.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", name");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append("?"); // subject
            sql.append(", ?"); // category
            sql.append(", ?"); // text
            sql.append(", ?"); // name
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", ?"); // user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            
            
            ps.setString(1, message.getSubject());
            ps.setString(2, message.getText());
            ps.setString(3, message.getCategory());
            ps.setString(4, message.getName());
            ps.setInt(5, message.getUserId());
            
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}