package shop.dao;

import static shop.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import shop.beans.Branch;
import shop.beans.Position;
import shop.beans.User;
import shop.exception.NoRowsUpdatedRuntimeException;
import shop.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch");
			sql.append(", position");
			sql.append(", status");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?"); // login
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch
			sql.append(", ?"); // position
			sql.append(", ?"); // status
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getPosition());
			ps.setInt(6, user.getStatus());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public User getUser(Connection connection, String login,
	        String password) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login = ? AND password = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, login);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	        	int id = rs.getInt("id");
	            String login = rs.getString("login");
	            String password = rs.getString("password");
	            String name = rs.getString("name");
	            int branch = rs.getInt("branch");
	            int position = rs.getInt("position");
	            int status = rs.getInt("status");
	            Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");

	            User user = new User();
	            user.setId(id);
	            user.setLogin(login);
	            user.setPassword(password);
	            user.setName(name);
	            user.setBranch(branch);
	            user.setPosition(position);
	            user.setStatus(status);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", branch = ?");
            sql.append(", position = ?");
            sql.append(", status = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getPosition());
            ps.setInt(6, user.getStatus());
            ps.setInt(7, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

	public List<Branch> getBranches(Connection conn) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toBranchList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException {
		List<Branch> ret = new ArrayList<>();

		while (rs.next()) {
			int branch_id = rs.getInt("branch_id");
			String branch_name = rs.getString("branch_name");

			Branch branch = new Branch();
			branch.setBranch_id(branch_id);
			branch.setBranch_name(branch_name);

			ret.add(branch);
		}

		return ret;
	}

	public List<Position> getPositions(Connection connp) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM positions";
			ps = connp.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			return toPositionList(rs);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Position> toPositionList(ResultSet rs) throws SQLException {
		List<Position> ret = new ArrayList<>();

		while (rs.next()) {
			int position_id = rs.getInt("position_id");
			String position_name = rs.getString("position_name");

			Position position = new Position();
			position.setPosition_id(position_id);
			position.setPosition_name(position_name);

			ret.add(position);
		}

		return ret;
	}
    public List<User> getUser(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login as login, ");
            sql.append("users.name as name, ");
            sql.append("users.branch as branch, ");
            sql.append("users.position as position, ");
            sql.append("branches.branch_id as branch_id, ");
            sql.append("branches.branch_name as branch_name, ");
            sql.append("positions.position_id as position_id, ");
            sql.append("positions.position_name as position_name, ");
            sql.append("users.status as status ");
            sql.append("FROM users ");
            sql.append("LEFT JOIN branches ");
            sql.append("ON branches.branch_id = users.branch ");
            sql.append("LEFT JOIN positions ");
            sql.append("ON positions.position_id = users.position ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toManagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toManagementList(ResultSet rs)
            throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String login = rs.getString("login");
                String name = rs.getString("name");
                int branch_id = rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                String position_name = rs.getString("position_name");
                String branch_name = rs.getString("branch_name");
                int status = rs.getInt("status");



                User user = new User();
                user.setId(id);
                user.setLogin(login);
                user.setName(name);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setBranch_name(branch_name);
                user.setPosition_name(position_name);
                user.setStatus(status);


                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }


	public  void getStatus(Connection connection , User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  status = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());


            ps.setInt(1, user.getStatus());
            ps.setInt(2, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}

