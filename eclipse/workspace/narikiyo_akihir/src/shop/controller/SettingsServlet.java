package shop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import shop.beans.User;
import shop.exception.NoRowsUpdatedRuntimeException;
import shop.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        UserService service = new UserService();
        request.setAttribute("branches", service.getBranches());
        request.setAttribute("positions", service.getPositions());

        User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));


        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("management.jsp").forward(request, response);
                return;
            }

            session.setAttribute("editUser", editUser);

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        UserService service = new UserService();
        request.setAttribute("branches", service.getBranches());
        request.setAttribute("positions", service.getPositions());

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin(request.getParameter("login"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setName(request.getParameter("name"));
        editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
        editUser.setPosition(Integer.parseInt(request.getParameter("position")));
        return editUser;
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String branch = request.getParameter("branch");
        String position =request.getParameter("position");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(login) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(branch) == true) {
            messages.add("支店名を入力してください");
        }
        if (StringUtils.isEmpty(position) == true) {
            messages.add("部署・役職名を入力してください");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名称を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}