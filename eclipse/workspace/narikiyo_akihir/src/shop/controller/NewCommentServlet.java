package shop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import shop.beans.Comment;
import shop.beans.User;
import shop.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> comment = new ArrayList<String>();

		if (isValid(request, comment) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comments = new Comment();
			comments.setText(request.getParameter("comment"));
			comments.setUserId(user.getId());
			comments.setMessageId(Integer.parseInt(request.getParameter("automessage")));

			new CommentService().register(comments , comments);

			response.sendRedirect("home");
		} else {
			session.setAttribute("errorMessage", comment);
			response.sendRedirect("home");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment) == true) {
			comments.add("メッセージを入力してください");
		}
		if (140 < comment.length()) {
			comments.add("140文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}