package shop.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import shop.beans.Comment;
import shop.beans.User;
import shop.beans.UserComment;
import shop.beans.UserMessage;
import shop.service.CommentService;
import shop.service.MessageService;


@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        List<UserMessage> messages = new MessageService().getMessage();
        List<UserComment> comments = new CommentService().getComment();

        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("comments", comments);


        request.getRequestDispatcher("home.jsp").forward(request, response);
       }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	Comment delete = new Comment();

    	delete.setDelete(Integer.parseInt(request.getParameter("delete_id")));
    	delete.setUserId(Integer.parseInt(request.getParameter("Userid")));

    	new CommentService().delete(delete);

		response.sendRedirect("home");
 }
}