package shop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import shop.beans.User;
import shop.service.UserService;

@WebServlet(urlPatterns = { "/registration" })
public class RegistrationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        UserService service = new UserService();
        request.setAttribute("branches", service.getBranches());
        request.setAttribute("positions", service.getPositions());
        request.getRequestDispatcher("registration.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin(request.getParameter("login"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setPosition(Integer.parseInt(request.getParameter("position")));

            new UserService().register(user);

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("registration");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String name = request.getParameter("name");
        String branch = request.getParameter("branch");
        String position =request.getParameter("position");

        if (StringUtils.isEmpty(login) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名称を入力してください");
        }
        if (StringUtils.isEmpty(branch) == true) {
            messages.add("支店名を入力してください");
        }
        if (StringUtils.isEmpty(position) == true) {
            messages.add("部署･役職名を入力してください");
        }
        
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}