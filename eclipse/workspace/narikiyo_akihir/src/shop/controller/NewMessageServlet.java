package shop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import shop.beans.Message;
import shop.beans.User;
import shop.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setSubject(request.getParameter("subject"));
            message.setText(request.getParameter("text"));
            message.setCategory(request.getParameter("category"));
            message.setName(user.getName());
            message.setUserId(user.getId());

            new MessageService().register(message);

            response.sendRedirect("home");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("post");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String message = request.getParameter("text");

        if (StringUtils.isEmpty(message) == true) {
            messages.add("メッセージを入力してください");
        }
        if (140 < message.length()) {
            messages.add("140文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}