
package shop.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import shop.beans.User;
import shop.service.UserService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<User> users = new UserService().getManagement();

        request.setAttribute("users", users);

        request.getRequestDispatcher("management.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	User status = new User();

    	status.setStatus(Integer.parseInt(request.getParameter("status")));
    	status.setId(Integer.parseInt(request.getParameter("id")));

    	new UserService().status(status);

		response.sendRedirect("management");

}
}