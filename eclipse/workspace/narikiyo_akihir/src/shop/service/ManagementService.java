package shop.service;

import static shop.utils.CloseableUtil.*;
import static shop.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import shop.beans.User;
import shop.dao.UserDao;

public class ManagementService {
	
    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    
	public List<User> getManagement() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<User> ret = userDao.getUser(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	
	
}
