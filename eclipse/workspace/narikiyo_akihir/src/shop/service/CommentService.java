package shop.service;

import static shop.utils.CloseableUtil.*;
import static shop.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import shop.beans.Comment;
import shop.beans.UserComment;
import shop.dao.CommentDao;
import shop.dao.UserCommentDao;

public class CommentService {

    public void register(Comment comment , Comment id) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserComment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentDao commentDao = new UserCommentDao();
            List<UserComment> ret = commentDao.getUserComments(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(Comment delete) {

	    Connection connection = null;
	    try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.getdelete(connection , delete);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}